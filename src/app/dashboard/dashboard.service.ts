import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
import { Headers } from '@angular/http';

@Injectable()
export class DashboardService {

  voucherUrl: string;

  constructor(private http: Http) {
    this.voucherUrl = environment.apiUrl;
   }

   buscarResumo():Promise<any>{
    const headers = new Headers();
    headers.append('Authorization', 'Basic Vm91Y2hlckFwaTpSM21AdDNj');

    return this.http.get(`${this.voucherUrl}resumo`, { headers: headers})
    .toPromise()
    .then(response => {
      return response.json()
    })
    .catch(erro => {
      return Promise.reject(`Erro ao carregar DashBoard!`)

    } )

   }

}
