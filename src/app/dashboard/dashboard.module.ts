import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard/dashboard.component';
import {  PanelModule } from 'primeng/primeng';
import {  ChartModule } from 'primeng/primeng';
import { DashboardService } from './dashboard.service';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
    ChartModule
  ],
  declarations: [DashboardComponent],
  exports: [DashboardComponent],
  providers: [DashboardService]
})
export class DashboardModule { }
