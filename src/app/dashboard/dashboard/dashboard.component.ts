import { Component, OnInit, ViewChild } from '@angular/core';
import { UIChart } from 'primeng/primeng';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  @ViewChild('chart') chart: UIChart;

  pieChartData : any;

constructor(private dashBoardService: DashboardService,
  private errorHandler: ErrorHandlerService) { }

ngOnInit() {
  this.configurarGraficoPizza();


  setInterval(() => {

    this.configurarGraficoPizza();
   // this.chart.reinit();
  }, 60000);

}


configurarGraficoPizza(){

  this.dashBoardService.buscarResumo()
  .then(dados => {

    this.chart.reinit();

    this.pieChartData = {

      labels : ['Empresas Cadastradas', 'Voucher Disponibilizados', 'Voucher Resgatados'],
      datasets: [ {

        data : [dados.totalEmpresasCadastradas, dados.totalVoucherDisponibilizados, dados.totalVoucherResgatados],
        backgroundColor: ['#109618', '#FF9900',  '#990099']
      }

      ]
    };

  })
  .catch( erro => this.errorHandler.handle(erro));
  ;

}

}
