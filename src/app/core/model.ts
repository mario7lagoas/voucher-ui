export class Empresa {
  id: number;
  cnpj: string;
  email: string;
  contato: string;
  cargo: string;
  telefone: string;
  voucher: string;
  resgatado: boolean;
  dataCadastro : Date;
  dataResgate: Date;

}

export class EmpresaPut{
  email: string;
  contato: string;
  cargo: string;
  telefone: string;
  resgatado: boolean;

}

export class Configuracao{
  assunto: string;
  statusPromocao: boolean;
  alertaVoucher: boolean;
}

export class Pesquisa{
  tipo: string;
  dado: string;

}
