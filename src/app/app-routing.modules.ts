import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ParamentroComponent } from "./configuracao/paramentro/paramentro.component";
import { PaginaNaoEncontradaComponent } from "./core/pagina-nao-encontrada.component";
import { EmpresaCadastroComponent } from "./empresa/empresa-cadastro/empresa-cadastro.component";
import { EmpresaFiltroComponent } from "./empresa/empresa-filtro/empresa-filtro.component";
import { EmpresaPesquisaComponent } from "./empresa/empresa-pesquisa/empresa-pesquisa.component";
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { RelatorioClientesComponent } from './relatorios/relatorio-clientes/relatorio-clientes.component';

const routes : Routes = [
  {path : '', redirectTo: 'home', pathMatch: 'full' },
  {path : 'empresas', component: EmpresaPesquisaComponent },
  {path : 'home', component: DashboardComponent },
  {path : 'empresas/:cnpj', component: EmpresaCadastroComponent },
  {path : 'configuracao', component: ParamentroComponent},
  {path : 'pesquisa', component: EmpresaFiltroComponent },
  {path : 'relatorio', component: RelatorioClientesComponent },
  {path : 'pagina-nao-encontrada', component: PaginaNaoEncontradaComponent },
  {path : '**', redirectTo: 'pagina-nao-encontrada' }
] ;

@NgModule({

  imports: [
    RouterModule.forRoot(routes,  { useHash: true })
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
