import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CpfCnpjPipe } from './cpf-cnpj.pipe';
import { TelefonePipe } from './telefone.pipe';

@NgModule({
  imports: [
    CommonModule

  ],
  exports:[
    CpfCnpjPipe,
    TelefonePipe
  ],
  declarations: [ CpfCnpjPipe, TelefonePipe]
})
export class PipesModule { }
