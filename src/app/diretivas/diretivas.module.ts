import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampoColoridoDirective } from './campo-colorido.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CampoColoridoDirective],
  exports: [CampoColoridoDirective]
})
export class DiretivasModule { }
