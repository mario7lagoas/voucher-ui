import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appCampoColorido]'
})
export class CampoColoridoDirective {

  @Input() public cor = 'yellow';

  @HostBinding('style.backgroundColor') corDeFundo : string;

  @HostListener("focus") aoGanharfoco(){
    this.corDeFundo = this.cor;
  }

  @HostListener('blur') aoPerderFoco(){
    this.corDeFundo = 'transparent';
  }

}
