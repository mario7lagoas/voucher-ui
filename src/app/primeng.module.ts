import { NgModule } from '@angular/core';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { ButtonModule } from 'primeng/components/button/button';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { SelectButtonModule } from 'primeng/components/selectbutton/selectbutton';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { InputMaskModule } from 'primeng/components/inputmask/inputmask';
import { RadioButtonModule } from 'primeng/components/radiobutton/radiobutton';
import { DialogModule } from 'primeng/components/dialog/dialog';



@NgModule({

  imports: [
    InputTextModule,
    ButtonModule,
    TooltipModule,
    CalendarModule,
    SelectButtonModule,
    DropdownModule,
    DataTableModule,
    InputMaskModule,
    RadioButtonModule,
    DialogModule
  ],
  exports: [

    InputTextModule,
    ButtonModule,
    TooltipModule,
    CalendarModule,
    SelectButtonModule,
    DropdownModule,
    DataTableModule,
    InputMaskModule,
    RadioButtonModule,
    DialogModule
  ],

})
export class PrimeNgModules { }
