import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ButtonModule } from 'primeng/components/button/button';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';

import { DiretivasModule } from '../diretivas/diretivas.module';
import { ParametroService } from './parametro.service';
import { SharedModule } from '../shared/shared.module';
import { ParamentroComponent } from './paramentro/paramentro.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    RouterModule,
    InputTextModule,
    DropdownModule,
    DiretivasModule,
    HttpModule,
    TooltipModule,
    SharedModule


  ],
  declarations: [ParamentroComponent],
  exports: [ParamentroComponent],
  providers: [ParametroService]
})
export class ConfiguracaoModule { }
