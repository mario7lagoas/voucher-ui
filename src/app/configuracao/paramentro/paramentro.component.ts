import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { ConfirmationService } from 'primeng/components/common/confirmationservice';

import { ToastyService } from 'ng2-toasty/src/toasty.service';

import { Configuracao } from '../../core/model';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { ParametroService } from '../parametro.service';

@Component({
  selector: 'app-paramentro',
  templateUrl: './paramentro.component.html',
  styleUrls: ['./paramentro.component.css']
})
export class ParamentroComponent implements OnInit {
  status = [
    {label: 'Promoção Ativa', value: true},
    {label: 'Promoção Inativa', value: false}
  ];

  statusAlerta = [
    {label: 'Enviar lembrete', value: true},
    {label: 'Não enviar lembrete', value: false}
  ];


  configuracao = new Configuracao;

  constructor(
    private parametroService : ParametroService,
    private toasty: ToastyService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar(){
    this.parametroService.consultar()
    .then(configuracao => {
      this.configuracao = configuracao;
      console.log(this.configuracao);
    })
    .catch( erro => this.errorHandler.handle(erro));
  }

  atualizar(form: FormControl){
    this.confirmation.confirm({message: 'Confirma a Alteração ?',
    accept: () => {

      this.parametroService.atualizarConfiguracao(this.configuracao)
        .then((c)=> {
          this.toasty.success('Configuração alterada com sucesso! ');

          })
        .catch( erro => this.errorHandler.handle(erro));
    }});

  }

}
