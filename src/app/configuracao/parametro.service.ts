import { Injectable } from '@angular/core';
import { Http , Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import { Configuracao } from '../core/model';

@Injectable()
export class ParametroService {

  voucherUrl: string;

  constructor(private http: Http) {
    this.voucherUrl = environment.apiUrl;
  }

    consultar(): Promise<any>{
      const headers = new Headers();
      headers.append('Authorization', 'Basic Vm91Y2hlckFwaTpSM21AdDNj');

      return this.http.get(`${this.voucherUrl}parametros`, { headers: headers } )
      .toPromise()
      .then(response => response.json())
      .catch(erro => {
        return Promise.reject(`Erro em buscar as configurações.`)
      })
    }

    atualizarConfiguracao( conf: Configuracao): Promise<any>{
      console.log("Meu envio service => " + JSON.stringify(conf));

      const headers = new Headers();
      headers.append('Authorization', 'Basic Vm91Y2hlckFwaTpSM21AdDNj');
      headers.append('Content-Type', 'application/json');


      return this.http.put(`${this.voucherUrl}parametros`, JSON.stringify(conf), { headers: headers } )
      .toPromise()
      .then(response => response.json())
      .catch(erro => {
        return Promise.reject(`Erro em alterar as configurações.`)
      })

    }


}
