import { FormControl } from '@angular/forms';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-message',
  template: `
  <div *ngIf="temErro()" class="ui-message ui-messages-error">
    {{ text }}
  </div>
  `,
  styles: []
})
export class MessageComponent {
  @Input() error: string;
  @Input() controle: FormControl;
  @Input() text: string;

  temErro(): boolean{
    return this.controle.hasError(this.error) && this.controle.dirty;
  }


}
