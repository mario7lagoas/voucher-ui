import { PrimeNgModules } from './primeng.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { CoreModule } from './core/core.module';
import { EmpresaModule } from './empresa/empresa.module';
import { DiretivasModule } from './diretivas/diretivas.module';
import { PipesModule } from './pipes/pipes.module';
import { ConfiguracaoModule } from './configuracao/configuracao.module';
import { AppRoutingModule } from './app-routing.modules';
import { DashboardModule } from './dashboard/dashboard.module';
import { RelatoriosModule } from './relatorios/relatorios.module';

@NgModule({
  declarations: [
    AppComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,

    AppRoutingModule,

    DiretivasModule,
    PipesModule,
    CoreModule,
    EmpresaModule,
    ConfiguracaoModule,
    DashboardModule,
    RelatoriosModule,

    PrimeNgModules

  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
