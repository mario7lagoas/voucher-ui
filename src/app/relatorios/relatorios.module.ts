import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ButtonModule } from 'primeng/components/button/button';

import { RadioButtonModule } from 'primeng/components/radiobutton/radiobutton';

import { SharedModule } from '../shared/shared.module';

import { RelatoriosService} from './relatorios.service';

import { RelatorioClientesComponent } from './relatorio-clientes/relatorio-clientes.component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    SharedModule,
    RouterModule,
    FormsModule,
    RadioButtonModule,

  ],
  declarations: [RelatorioClientesComponent],
  exports:[RelatorioClientesComponent],
  providers: [RelatoriosService]
})
export class RelatoriosModule { }
