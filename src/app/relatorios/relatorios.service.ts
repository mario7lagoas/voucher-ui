import { Injectable } from '@angular/core';

import { Http , Headers, ResponseContentType } from '@angular/http';
import { environment } from '../../environments/environment';


@Injectable()
export class RelatoriosService {

  voucherUrl: string;

  constructor(private http: Http) {
    this.voucherUrl = environment.apiUrl;
   }

  relatorioDeClientes(dado : string){
    const headers = new Headers();
    headers.append('Authorization', 'Basic Vm91Y2hlckFwaTpSM21AdDNj');
    headers.append('dado', dado);

    return this.http.get(`${this.voucherUrl}relatorio/clientes`, { headers: headers,
                                                responseType: ResponseContentType.Blob } )
    .toPromise()
    .then(response => response.blob())
    .catch(erro => {
      return Promise.reject(`Erro em Solicitar Relatório.`)
    })

  }
/*
  downloadPdfRelatorio(dado : string){

    const headers = new Headers();
    headers.append('Authorization', 'Basic Vm91Y2hlckFwaTpSM21AdDNj');
    headers.append('dado', dado);

    return this.http.get(`${this.voucherUrl}relatorio/clientes64`, { headers: headers,
      responseType : ResponseContentType.Text} )

    .subscribe(data => {
      document.querySelector('iframe').src = data;
    });

  }
*/
}
