import { Component, OnInit } from '@angular/core';
import { ToastyService } from 'ng2-toasty/src/toasty.service';

import { RelatoriosService } from '../relatorios.service';

import { ErrorHandlerService } from './../../core/error-handler.service';


@Component({
  selector: 'app-relatorio-clientes',
  templateUrl: './relatorio-clientes.component.html',
  styleUrls: ['./relatorio-clientes.component.css']
})
export class RelatorioClientesComponent implements OnInit {

  relatorioSelecionado: any = null;

      visible: boolean = false;

  tipoSelecionados: any[] = [
    { name: 'Todos Clientes', key: 'ALL' },
    { name: 'Voucher Resgatados', key: 'RESGATADO' },
    { name: 'Voucher Pendentes', key: 'PENDENTE' }
  ];


  constructor(
    private toasty: ToastyService,
    private errorHandler: ErrorHandlerService,
    private relatorioService : RelatoriosService
  ) { }

  ngOnInit() {
    this.relatorioSelecionado = this.tipoSelecionados[0];
  }




    showDialog() {
        this.visible = true;
    }

  gerar(){
    this.relatorioService.relatorioDeClientes(this.relatorioSelecionado.key)
    .then(relatorio => {


      const url = window.URL.createObjectURL(relatorio);

      var link = document.createElement('a');
      link.href = url;
      link.download = `RELATORIO_CLIENTES_${this.relatorioSelecionado.key}.pdf`;

      link.click();

     // window.open(url);

    })
    .catch( erro => this.errorHandler.handle(erro));

  }


}
