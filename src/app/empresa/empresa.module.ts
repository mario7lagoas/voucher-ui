import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';


import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { ButtonModule, CalendarModule, DataTableModule, DropdownModule, InputMaskModule, InputTextModule, SelectButtonModule} from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/components/radiobutton/radiobutton';


import { PipesModule } from './../pipes/pipes.module';
import { EmpresaService } from './empresa.service';
import { EmpresaPesquisaComponent } from './empresa-pesquisa/empresa-pesquisa.component';
import { DiretivasModule } from '../diretivas/diretivas.module';
import { EmpresaCadastroComponent } from './empresa-cadastro/empresa-cadastro.component';
import { EmpresaFiltroComponent } from './empresa-filtro/empresa-filtro.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    DataTableModule,
    ButtonModule,
    InputTextModule,
    TooltipModule,
    PipesModule,
    DiretivasModule,
    RouterModule,
    SelectButtonModule,
    DropdownModule,
    CalendarModule,
    InputMaskModule,
    SharedModule,
    RadioButtonModule


  ],
  declarations: [EmpresaPesquisaComponent, EmpresaCadastroComponent, EmpresaFiltroComponent ],
  exports:[EmpresaPesquisaComponent, EmpresaCadastroComponent, EmpresaFiltroComponent],
  providers: [EmpresaService]
})
export class EmpresaModule { }
