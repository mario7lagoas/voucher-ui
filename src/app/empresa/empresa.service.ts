import { environment } from './../../environments/environment';
import { EmpresaPut, Pesquisa } from './../core/model';
import { Http , Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import * as moment from 'moment';
import { Empresa } from '../core/model';

export class EmpresaFiltro {

  contato: string;
  resgatado: string;
  ordenacao:string;
  dataInicio: Date;
  dataFim: Date;
  pagina = 0;
  itensPorPagina = 5;

}

@Injectable()
export class EmpresaService {

  voucherUrl: string;

  constructor(private http: Http) {
    this.voucherUrl = environment.apiUrl;
   }

  listar(filtro: EmpresaFiltro): Promise<any>{
    const params = new URLSearchParams();
    const headers = new Headers();
    headers.append('Authorization', 'Basic Vm91Y2hlckFwaTpSM21AdDNj');

    params.set('page', filtro.pagina.toString());
    params.set('size', filtro.itensPorPagina.toString());

    if(filtro.contato){
      params.set('contato' , filtro.contato);
    }

    console.log(filtro.resgatado);

    if(filtro.resgatado != "undefined"){
      params.set('resgatado', filtro.resgatado);
    }

    if(filtro.ordenacao){
      params.set('sort', filtro.ordenacao);

    }

    if(filtro.dataInicio){
      params.set('de', moment(filtro.dataInicio).format('YYYY-MM-DD'));
    }

    if(filtro.dataFim){
      params.set('ate', moment(filtro.dataFim).format('YYYY-MM-DD'));
    }

    return this.http.get(`${this.voucherUrl}filtros`, { headers: headers, search: params } )
    .toPromise()
    .then(response => {
      const responseJson = response.json();
      const empresas = response.json().content;

      const resultado = {
        empresas: empresas,
        total: responseJson.totalElements,
      };

      return resultado;

    })
    .catch(erro => {
      return Promise.reject(`Erro em buscar o empresa!`)

    } )
  }

  pesquisarEmpresaCnpj(cnpj : string): Promise<any>{

    const headers = new Headers();
    headers.append('Authorization', 'Basic Vm91Y2hlckFwaTpSM21AdDNj');

    return this.http.get(`${this.voucherUrl}cadastros/${cnpj}`, { headers: headers } )
    .toPromise()
    .then(response => response.json())
    .catch( erro => {
      return Promise.reject(erro)

    })
  }

  pesquisar(cnpj : string): Promise<any>{

    const headers = new Headers();
    headers.append('Authorization', 'Basic Vm91Y2hlckFwaTpSM21AdDNj');

    return this.http.get(`${this.voucherUrl}cadastros/${cnpj}`, { headers: headers } )
    .toPromise()
    .then(response => response.json())
    .catch( erro => {
      return Promise.reject(erro)

    })
  }

  buscar(pesquisa : Pesquisa): Promise<any>{

    const headers = new Headers();
    var dado = pesquisa.dado;
    headers.append('Authorization', 'Basic Vm91Y2hlckFwaTpSM21AdDNj');

    if(pesquisa.tipo == "voucher"){
      dado = dado + "/voucher";
    }

    return this.http.get(`${this.voucherUrl}cadastros/${dado}`, { headers: headers } )
    .toPromise()
    .then(response => response.json())
    .catch( erro => {
      if (erro.status === 404  && erro.statusText === 'OK' ){
          return Promise.reject(`[${pesquisa.dado}] - Não encontrado!`);
      }

      return Promise.reject(erro);

    })
  }



  atualizarEmpresa(empresa: Empresa): Promise<any>{
    const headers = new Headers();
    headers.append('Authorization', 'Basic Vm91Y2hlckFwaTpSM21AdDNj');
    headers.append('Content-Type', 'application/json');

    const empresaAlteracao = new EmpresaPut;
    empresaAlteracao.email = empresa.email;
    empresaAlteracao.contato = empresa.contato;
    empresaAlteracao.cargo = empresa.cargo;
    empresaAlteracao.telefone = empresa.telefone;
    empresaAlteracao.resgatado = empresa.resgatado;


    console.log(empresaAlteracao);


    return this.http.put(`${this.voucherUrl}cadastros/${empresa.cnpj}`, JSON.stringify(empresaAlteracao), { headers: headers })
      .toPromise()
      .then(response => {
        console.log(response);
      })
      .catch( erro => {
        return Promise.reject(erro)

      })

  }




}
