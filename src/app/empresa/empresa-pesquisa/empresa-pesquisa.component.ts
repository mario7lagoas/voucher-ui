import { EmpresaFiltro, EmpresaService } from './../empresa.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { LazyLoadEvent, ConfirmationService } from 'primeng/primeng';
import { ToastyService } from 'ng2-toasty';
import { ErrorHandlerService } from '../../core/error-handler.service';

@Component({
  selector: 'app-empresa-pesquisa',
  templateUrl: './empresa-pesquisa.component.html',
  styleUrls: ['./empresa-pesquisa.component.css']
})
export class EmpresaPesquisaComponent implements OnInit {

  totalRegistros = 0;
  filtro = new EmpresaFiltro();
  @ViewChild('tabela') grid;

  empresas =[];
  //contato: string;
 // resgatado: string;
 // De: Date;
 // Ate: Date;

  status = [
    {label: 'Todos', value: undefined},
    {label: 'Resgatado', value: true},
    {label: 'Pendente', value: false}
  ];

  ordenacao = [
    {label: 'Contato', value: 'contato'},
    {label: 'CNPJ', value: 'cnpj'},
    {label: 'E-mail', value: 'email'},

  ];


  constructor(
    private empresaService: EmpresaService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService

  ) { }

  ngOnInit() {
 //   this.pesquisar();
  }

  pesquisar(pagina = 0){

    this.filtro.pagina = pagina;

    this.empresaService.listar(this.filtro)
    .then(resultado => {
      this.totalRegistros = resultado.total;
      this.empresas = resultado.empresas;
      console.log(this.empresas);
    })
    .catch( erro => this.errorHandler.handle(erro));
  }

  aoMudarPagina(event: LazyLoadEvent){
    //pega pagina atual
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);

  }

}
