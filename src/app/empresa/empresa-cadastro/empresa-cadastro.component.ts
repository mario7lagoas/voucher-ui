import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastyService } from 'ng2-toasty/src/toasty.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { Empresa } from '../../core/model';
import { EmpresaService } from '../empresa.service';

@Component({
  selector: 'app-empresa-cadastro',
  templateUrl: './empresa-cadastro.component.html',
  styleUrls: ['./empresa-cadastro.component.css']
})
export class EmpresaCadastroComponent implements OnInit {
  status = [
    {label: 'Resgatado', value: true},
    {label: 'Pendente', value: false}
  ];

  empresa = new Empresa();


  constructor(private toasty: ToastyService,
    private empresaService: EmpresaService,
    private route: ActivatedRoute,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService) { }

  ngOnInit() {

    const cnpjEmpresa = this.route.snapshot.params['cnpj'];

    if(cnpjEmpresa){
      this.carregarEmpresa(cnpjEmpresa);
    }
  }

  atualizar(form: FormControl) {

    this.confirmation.confirm({message: 'Confirma a Alteração ?',
    accept: () => {

        this.empresaService.atualizarEmpresa(this.empresa)
          .then(() => {
            this.toasty.success('Empresa alterada com sucesso!');
      //  form.reset();
      //  this.empresa = new Empresa();

          })
          .catch( erro => this.errorHandler.handle(erro));

    }});

  }

  carregarEmpresa(cnpj: string){
    this.empresaService.pesquisarEmpresaCnpj(cnpj)
      .then(empresa => {
        console.log(empresa);
        this.empresa = empresa;

      })
      .catch( erro => this.errorHandler.handle(erro));
  }
}
