import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';

import { ToastyService } from 'ng2-toasty/src/toasty.service';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { Pesquisa } from '../../core/model';
import { EmpresaFiltro, EmpresaService } from '../empresa.service';

@Component({
  selector: 'app-empresa-filtro',
  templateUrl: './empresa-filtro.component.html',
  styleUrls: ['./empresa-filtro.component.css']
})
export class EmpresaFiltroComponent implements OnInit {

  totalRegistros = 0;
  filtro = new EmpresaFiltro();
  relatorioPesquisa: any = null;

  @ViewChild('tabela') grid;

  pesquisa = new Pesquisa();


  tipos = [
    { label: 'CNPJ', value: 'cnpj' },
    { label: 'VOUVHER', value: 'voucher' },
  ];

  tipoSelecionados: any[] = [
    { name: 'CNPJ', key: 'cnpj' },
    { name: 'Voucher', key: 'voucher' }
  ];

  empresas =[];
  constructor(
    private empresaService: EmpresaService,
    private toasty: ToastyService,
    private errorHandler: ErrorHandlerService
    ) { }

  ngOnInit() {
    this.relatorioPesquisa = this.tipoSelecionados[0];
  }

  get tipoConsulta(){
    return

  }
  consultar(form: FormControl){
    this.pesquisa.tipo = this.relatorioPesquisa.key;
    this.empresaService.buscar(this.pesquisa)
    .then(resultado => {

      const res = [];
      res.push(resultado);
      this.empresas =  res;
    })
    .catch( erro => this.errorHandler.handle(erro));

  }

  pesquisar(pagina = 0){

    this.filtro.pagina = pagina;

    console.log(this.filtro);

    this.empresaService.listar(this.filtro)
    .then(resultado => {
      this.totalRegistros = resultado.total;
      this.empresas = resultado.empresas;
      console.log(this.empresas);
    })
    .catch( erro => this.errorHandler.handle(erro));

  }

  aoMudarPagina(event: LazyLoadEvent){
    //pega pagina atual
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);
    console.log(event);

  }

}
